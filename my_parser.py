#!/usr/bin/env python3

from vosk import Model, KaldiRecognizer, SetLogLevel
import sys
import os
import wave
import json
from tqdm import tqdm
from my_converter import json_to_html

def main():
    # Voeg voor elk interview dat je wilt parsen het volgende toe aan de lijst in dezelfde volgorde:

    # de interview nummers van het veteranen vertellen interview
    interview_ids = ["202","1713","1714"]

    #de link naar de originele audio of het pad naar de audio
    web_audio_sources = [
        "https://veteranenvertellen.nl/content/audio/D4GNDKDG.mp3",
        "https://veteranenvertellen.nl/content/audio/VUJC4EKT.mp3",
        "https://veteranenvertellen.nl/content/audio/O9RYNZ95.mp3"

    ]
    # de titel die je het interview wilt geven
    titles = [
        "Interview met UNIFIL veteraan in Libanon uitgezonden als Dienstplicht Koninklijke Landmacht, Hulprichter 120 mm mortier op Post 7-7 bij Zibqin",
        "Interview met vrouwelijke veteraan die uitgezonden werd naar Voormalig Joegoslavië, heeft de diagnose PTSS en heeft deelgenomen aan de Invictus Games.",
        "Interview met Libanonveteraan, uitgezonden naar Libanon in 1980. Kreeg de diagnose PTSS en voor zijn herstel loopt hij jaarlijks mee aan de Nijmeegse Vierdaagse."
        ]
    
    # het pad naar de audio in .wav of .wav format
    input_file_paths = [
        r"E:\thesis_audio\sessie 3\202.wav",
        r"E:\thesis_audio\sessie 3\1713.wav",
        r"E:\thesis_audio\sessie 3\1714.wav"
        ]
    
    parse_multiple(interview_ids,web_audio_sources,titles,input_file_paths)

def parse_multiple(output_file_names_list, audio_source_list, title_list, input_file_path_list):
    web_source_base = "https://www.veteranenvertellen.nl/vi/interview/"
    
    for i in tqdm(range(len(output_file_names_list))):
        output_name = output_file_names_list[i]
        audio_source = audio_source_list[i]
        title = title_list[i]
        input_file_path = input_file_path_list[i]
        
        parse(input_file_path, output_name)

        web_source = web_source_base+"{id}/".format(id=output_name)
        json_to_html(output_name+".json",output_name+".html", audio_source, title=title,web_source=web_source)


def parse(input_filepath, output_file_names="output"):

    wf = wave.open(input_filepath, "rb")
    if wf.getnchannels() != 1 or wf.getsampwidth() != 2 or wf.getcomptype() != "NONE":
        print ("Audio file must be WAV format mono PCM.")
        exit (1)

    model = Model("model")
    rec = KaldiRecognizer(model, wf.getframerate())

    parse_to_file(rec, wf, output_file_names)


def parse_to_file(rec, wf, filename):

    text_file = filename+".txt"

    json_file = filename+".json"
    with open(json_file, 'a') as f:
        while True:
            data = wf.readframes(4000)
            if len(data) == 0:
                break
            if rec.AcceptWaveform(data):
                res = json.loads(rec.Result())
                f.writelines(json.dumps(res)+"\n")
                #print("\n"+res["text"]) #uncomment to read during parsing
        #else:
            #print(rec.PartialResult())
    # print("######## Result ########")
    # final_result = json.loads(rec.FinalResult())
    # print(final_result)
    # print("####### ####### ########")

    make_txt_file(json_file,text_file)

def make_txt_file(raw_file_path, target_file_name):

    with open(raw_file_path, 'r') as f:
        lines = f.readlines()
    
    with open(target_file_name, 'a') as f:
        for line in lines:
            datapoint = json.loads(line)
            f.writelines("\n"+datapoint["text"])

if __name__ == "__main__":
    SetLogLevel(0)

    if not os.path.exists("model"):
        print ("Please download the model from https://alphacephei.com/vosk/models and unpack as 'model' in the current folder.")
        exit (1)
    main()