import json
from tqdm import tqdm
import os


def main():
    json_to_html("202.json","202.html", "https://veteranenvertellen.nl/bron/audio/D4GNDKDG.mp3")

def json_to_html(json_file_path, target_file_name, audio_source, title="Interview", web_source="https://www.veteranenvertellen.nl"):
    with open(json_file_path, 'r') as f:
        lines = f.readlines()

    with open(target_file_name, 'a') as f:
        f.write(html_head(audio_source, title, web_source))

        for line in lines:
            f.write(html_text_line(line))

        f.write(html_foot())

def html_head(audio_source, title, web_source):
    opening = "<!DOCTYPE html>\n<html>"

    #included in head
    style = "<style>body{text-align:center}*{font-family:'Lucida Sans','Lucida Sans Regular','Lucida Grande','Lucida Sans Unicode',Geneva,Verdana,sans-serif}section{ width:90%;margin:5px auto;border-radius:3px;padding:3px;background-color:#e0ffff;transition:margin .2s}section p{margin:10px;text-align:justify}section:hover{margin:2px auto 8px auto}section.active{background-color:#0ff;size:15em}audio{width:97%;margin:0 auto;z-index:1}.sticky{position:-webkit-sticky;position:sticky;top:0}@media only screen and (min-width:768px){section{max-width:60%}audio{max-width:75%}}.tooltip{position:relative;display:inline-block;border-bottom:1px dotted #000}.tooltip .tooltiptext{visibility:hidden;width:120px;background-color:#555;color:#fff;text-align:center;border-radius:6px;padding:5px 0;position:absolute;z-index:1;bottom:125%;left:50%;margin-left:-60px;opacity:0;transition:opacity .3s}.tooltip .tooltiptext::after{content:"";position:absolute;top:100%;left:50%;margin-left:-5px;border-width:5px;border-style:solid;border-color:#555 transparent transparent transparent}.tooltip:hover .tooltiptext{ visibility:visible;opacity:1}</style>"
    script= "<script>function setAudio(e){document.getElementById(\"audio1\").currentTime=e}function getSections(){return document.getElementsByClassName(\"transcription\")}</script>"

    head  = "<head><title>{tit}</title><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">{styl}{scrip}</head><body>".format(tit=title, styl=style,scrip=script)
    
    #inside of body
    titel = "<h1>{tit}</h1>".format(tit=title)
    audio = "<audio class=\"sticky\" controls id=\"audio1\" preload=\"auto\" src=\"{src}\" ><p>Your browser does not support the audio element</p></audio>".format(src=audio_source)
    link_to_source = "<a href ={web_sourc}>naar veteranen vertellen</a>".format(web_sourc=web_source)

    return opening + "\n" + head + "\n" + titel + "\n" + audio + "\n" + link_to_source

def html_foot():
    return "</body>\n</html>"
def html_text_line(line):
    datapoint = json.loads(line)
    try:
        result = datapoint["result"]
    except:
        return ""
    startTime = result[0]["start"]
    endTime = result[-1]["end"]
    currentText = datapoint["text"].replace("<unk>","&lt;unk&gt;")
    return "\n<section id=\"{start}\" class=\"transcription tooltip\" style=\"border: 5px;\" onclick=\"setAudio({start})\"><span class=\"tooltiptext\">{startShort} - {endShort}s</span><p>{text}</p></section>".format(start=startTime,startShort=round(startTime),endShort=round(endTime),text=currentText)

if __name__ == "__main__":
    main()