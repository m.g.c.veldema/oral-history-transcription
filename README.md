# Oral history transciptor
> Code for my bachelor's thesis in artificial intelligence at Utrecht University

Code to generate a transcriptions for oral history audio interviews. Used for interviews of Dutch UNIFIL veterans of https://veteranenvertellen.nl/ by paralinguistic analysis.

## Links
